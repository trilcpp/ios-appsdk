//
//  trillTech.h
//  trillSDK
//
//  Created by ShivanshJ on 11/14/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <CoreAudioKit/CoreAudioKit.h>

#import <AudioToolbox/Audiotoolbox.h>
#import <AudioToolbox/AudioQueue.h>
#import <AudioToolbox/AudioFile.h>



//-------Delegate
@protocol TransferDataCallBackDelegate <NSObject>
-(void)onTriggerRecieved : (int) triggerCode;
-(void)onToneDecrypted: (int) finalCode : (bool)isLocation;

@end //end protocol


//-------Original class interface
@interface trillSDK : NSObject <AVAudioSessionDelegate> 
@property (nonatomic, retain) id <TransferDataCallBackDelegate> delegate;

- (void)initializeSDK:(NSString*) security_token:(NSString *)name_of_package completionHandler: ( void (^)(void) )complete;
- (void)infoCallBack;
- (void)SDKdestructor;

@end    //end interface

