//
//  ViewController.m
//  trillAPP_SDK
//
//  Created by Mac Mini on 11/27/17.
//  Copyright © 2017 Mac Mini. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    _view1.hidden = YES;
    
    _view1.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    _view1.layer.shadowOpacity = 0.4;
    //----WebView
    NSString *urlAddress = @"http://54.169.234.17/golocoweb/heatmap.php";
    NSURL *url = [[NSURL alloc] initWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webview loadRequest:requestObj];
    
    //----SDK initialize
    trillSDK *tSDK = [[trillSDK alloc] init];
    tSDK.delegate = self;
    [tSDK initializeSDK:@"4d8c10f9-3b80-48a9-902e-a78dace8963e" :@"com.testing.sdk" completionHandler:^{
        [tSDK infoCallBack];
    }];
}

-(void)onToneDecrypted: (int) finalCode : (bool)isLocation{
    _view1.hidden = NO;
    NSString *message = [NSString alloc];
    switch(finalCode){
        case 1324:
            message = @"50% OFF on BEER for the next 30 mins";
            break;
        case 2321:
            message = @"Special price on PIZZA's - Rs.150 ";
            break;
        case 4132:
            message = @"Invite your friend to the APP & get 10% OFF on your bill";
            break;
        case 4341:
            message = @"Travel Bonanza, MakeMyTrip Offer";
            break;
        default:
            message = @"Welcome to TrillZone";
            break;
}
    
    //---Animation
    UILabel __block *newlabel = [[UILabel alloc]initWithFrame: CGRectMake(0, 0, 400, 100)];
    UIFont * customFont = [UIFont fontWithName:@"Futura" size:15];
    newlabel.font = customFont;
    
    // animation 1
    [UIView animateKeyframesWithDuration:0.5
                                   delay:0.0
                                 options:_animation1
                              animations:^{
                                   newlabel.text = message;
                                  _view1.frame = CGRectMake(_view1.frame.origin.x - 500, _view1.frame.origin.y, _view1.frame.size.width, _view1.frame.size.height);
                              }
                              completion:^(BOOL finished) {
                                  [UIView animateKeyframesWithDuration:0.7
                                                                 delay:2.0
                                                               options:_animation1
                                                            animations:^{
                                                                _view1.frame = CGRectMake(_view1.frame.origin.x - 500, _view1.frame.origin.y, _view1.frame.size.width, _view1.frame.size.height);
                                                            }
                                                            completion:^(BOOL finished) {
                                                                _animationInProgress = NO;
                                        
                                                            }];
                              }];

//    [newlabel setCenter:_view1.center];
    
    [_view1 addSubview:newlabel];
    
    //----Print Code
    NSLog(@"\n\n\n\nThis has the decrypted trillTone code %d \n\n\n\n", finalCode);
    self.label.text = [NSString stringWithFormat:@"%d", finalCode];
}
-(void) onTriggerRecieved:(int)triggerCode{
    NSLog(@"\n--SDK_message : Location %d\n",triggerCode);
    self.label.text = [NSString stringWithFormat:@"%d", triggerCode];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
