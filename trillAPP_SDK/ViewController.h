//
//  ViewController.h
//  trillAPP_SDK
//
//  Created by Mac Mini on 11/27/17.
//  Copyright © 2017 Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <trillSDK/trillTech.h>
@interface ViewController : UIViewController <TransferDataCallBackDelegate>

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIWebView *webview;

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (nonatomic, assign) UIViewAnimationOptions animation1;
@property (nonatomic, assign) UIViewAnimationOptions animation2;
@property (nonatomic, assign) BOOL animationInProgress;

@end

