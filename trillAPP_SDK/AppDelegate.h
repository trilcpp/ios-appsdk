//
//  AppDelegate.h
//  trillAPP_SDK
//
//  Created by Mac Mini on 11/27/17.
//  Copyright © 2017 Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

